package com.farhan.cuti.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.farhan.cuti.model.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long>{

}
