package com.farhan.cuti.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.farhan.cuti.model.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{
	@Query(value = "SELECT * FROM bucket_approval where id_user_leave_request IN (SELECT id_user_leave_request FROM user_leave_request WHERE id_user = :userId)", nativeQuery = true)
	Page<BucketApproval> findByReqByUser(@Param("userId") Long userId, Pageable pageable);
}
