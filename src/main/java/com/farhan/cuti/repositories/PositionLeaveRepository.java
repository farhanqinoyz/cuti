package com.farhan.cuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.farhan.cuti.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long>{

}
