package com.farhan.cuti.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.farhan.cuti.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{

}
