package com.farhan.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhan.cuti.dtomodel.UsersDTO;
import com.farhan.cuti.model.Users;
import com.farhan.cuti.repositories.UsersRepository;

@RestController
@RequestMapping("api/users")
public class UsersController {
	@Autowired
	UsersRepository usersRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllUsers(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Users> listUsers= usersRepository.findAll();
		List<UsersDTO> listUsersDTO = new ArrayList<UsersDTO>();
		
		for(Users users : listUsers) {
			UsersDTO dto = modelMapper.map(users, UsersDTO.class);
			listUsersDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Users success!");
		result.put("Data", listUsersDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateUsers(@Valid @RequestBody UsersDTO usersDTO){
		ModelMapper modelMapper = new ModelMapper();
			Users users = modelMapper.map(usersDTO, Users.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Users success!");
		result.put("Data", usersRepository.save(users));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateUsers(@Valid @RequestBody UsersDTO usersDTO,
			@PathVariable(value = "id") Long idUsers){

		Map<String, Object> result = new HashMap<String, Object>(); 

		ModelMapper modelMapper = new ModelMapper();
		Users usersById = usersRepository.findById(idUsers).get();
		Date createdDate = usersById.getCreatedDate();
		String createBy = usersById.getCreatedBy();
		
		usersById = modelMapper.map(usersDTO,Users.class);
		
		usersById.setIdUser(idUsers);
		usersById.setCreatedDate(createdDate);
		usersById.setCreatedBy(createBy);
		
		usersRepository.save(usersById);
		result.put("Status", 200);
		result.put("Message", "Update data Users success!");
		result.put("Data", usersById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idUsers) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Users users = usersRepository.findById(idUsers).get();
    	
        usersRepository.delete(users);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record success!");
		result.put("ID : ",""+idUsers);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
