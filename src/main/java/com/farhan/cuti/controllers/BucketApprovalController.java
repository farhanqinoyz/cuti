package com.farhan.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhan.cuti.dtomodel.BucketApprovalDTO;
import com.farhan.cuti.dtomodel.PositionDTO;
import com.farhan.cuti.dtomodel.UserLeaveRequestDTO;
import com.farhan.cuti.dtomodel.UsersDTO;
import com.farhan.cuti.model.UserLeaveRequest;
import com.farhan.cuti.model.Users;
import com.farhan.cuti.model.BucketApproval;
import com.farhan.cuti.model.Position;
import com.farhan.cuti.repositories.BucketApprovalRepository;
import com.farhan.cuti.repositories.PositionLeaveRepository;
import com.farhan.cuti.repositories.PositionRepository;
import com.farhan.cuti.repositories.UserLeaveRequestRepository;
import com.farhan.cuti.repositories.UsersRepository;

@RestController
@RequestMapping("api/bucketApproval")
public class BucketApprovalController {
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@Autowired
	PositionRepository positionRepository;
	
	@PostMapping("/approve")
	public Map<String, Object> BucketApproval(@Valid @RequestBody BucketApprovalDTO bucketApprovalDTO){
		
		Map<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		
		List<Position> listPosition = positionRepository.findAll(Sort.by(Sort.Direction.ASC, "namePosition"));
		List<PositionDTO> listPositionDTO = new ArrayList<PositionDTO>();
		
		Users users = usersRepository.findById(bucketApprovalDTO.getUsers().getIdUser()).get();
		UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);
		
		UserLeaveRequest userLeaveRequest = userLeaveRequestRepository.findById(bucketApprovalDTO.getUserLeaveRequest().getIdUserLeaveRequest()).get();
		UserLeaveRequestDTO userLeaveRequestDTO = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
		
		bucketApprovalDTO.setRequesterName(userLeaveRequestDTO.getCreatedBy());
		bucketApprovalDTO.setDescription(userLeaveRequestDTO.getDescription());
		BucketApproval bucketApprovalModelMapper = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
		
		Boolean error = true;
		Date dateToday = new Date();
		Long differencesDate = Math.abs(userLeaveRequestDTO.getLeaveDateFrom().getTime() - userLeaveRequestDTO.getLeaveDateTo().getTime());
		Long diff = TimeUnit.DAYS.convert(differencesDate, TimeUnit.MILLISECONDS);
		int remainingDays = userLeaveRequestDTO.getRemainingDaysOff();
		int resultDays = 0;
		
		if(bucketApprovalDTO.getStatus().equalsIgnoreCase("approved")) {
			resultDays = remainingDays - Math.toIntExact(diff);
		}else {
			resultDays = remainingDays;			
		}
		
		for(Position position: listPosition) {
			PositionDTO dto = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(dto);
		}
		
		userLeaveRequestDTO.setRemainingDaysOff(resultDays);
		userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
		if(userLeaveRequestDTO.getLeaveDateFrom().getTime()  > bucketApprovalDTO.getDecisionDate().getTime()){
			result.put("Message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
		}else {
			error = false;
		}
		
		if(usersDTO.getPosition().getNamePosition().equalsIgnoreCase(listPositionDTO.get(2).getNamePosition())) {
			if(usersDTO.getPosition().getNamePosition().equalsIgnoreCase(userLeaveRequestDTO.getUsers().getPosition().getNamePosition())) {
				error=true;
			}
		}else if(usersDTO.getPosition().getNamePosition().equalsIgnoreCase(listPositionDTO.get(0).getNamePosition())) {
			error=true;
			
		}else if(usersDTO.getPosition().getNamePosition().equalsIgnoreCase(listPositionDTO.get(1).getNamePosition())) {
			
		}
		
		
		System.out.println(error);
		
		if(!error) {
			result.put("Message", "Permohonan dengan ID "+bucketApprovalDTO.getUserLeaveRequest().getIdUserLeaveRequest()+" telah berhasil diputuskan.");
			userLeaveRequestRepository.save(userLeaveRequest);		
			bucketApprovalRepository.save(bucketApprovalModelMapper);		
		}
		result.put("Data",bucketApprovalDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	public List<UsersDTO> MappingUsers(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<Users> listUsers = usersRepository.findAll();
		List<UsersDTO> listUsersDTO = new ArrayList<UsersDTO>();
		for(Users users : listUsers) {
			UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);
			listUsersDTO.add(usersDTO);
		}
		return listUsersDTO;
	}
	
}
