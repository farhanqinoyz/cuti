package com.farhan.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhan.cuti.dtomodel.PositionLeaveDTO;
import com.farhan.cuti.model.PositionLeave;
import com.farhan.cuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("api/positionLeave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPositionLeave(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeave> listPositionLeave= positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<PositionLeaveDTO>();
		
		for(PositionLeave positionLeave : listPositionLeave) {
			PositionLeaveDTO dto = modelMapper.map(positionLeave, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data PositionLeave success!");
		result.put("Data", listPositionLeaveDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePositionLeave(@Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		ModelMapper modelMapper = new ModelMapper();
			PositionLeave positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data PositionLeave success!");
		result.put("Data", positionLeaveRepository.save(positionLeave));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePositionLeave(@Valid @RequestBody PositionLeaveDTO positionLeaveDTO,
			@PathVariable(value = "id") Long idPositionLeave){

		Map<String, Object> result = new HashMap<String, Object>(); 

		ModelMapper modelMapper = new ModelMapper();
		PositionLeave positionLeaveById = positionLeaveRepository.findById(idPositionLeave).get();
		Date createdDate = positionLeaveById.getCreatedDate();
		String createBy = positionLeaveById.getCreatedBy();
		
		positionLeaveById = modelMapper.map(positionLeaveDTO,PositionLeave.class);
		
		positionLeaveById.setIdPositionLeave(idPositionLeave);
		positionLeaveById.setCreatedDate(createdDate);
		positionLeaveById.setCreatedBy(createBy);
		
		positionLeaveRepository.save(positionLeaveById);
		result.put("Status", 200);
		result.put("Message", "Update data PositionLeave success!");
		result.put("Data", positionLeaveById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idPositionLeave) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PositionLeave positionLeave = positionLeaveRepository.findById(idPositionLeave).get();
    	
        positionLeaveRepository.delete(positionLeave);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record success!");
		result.put("ID : ",""+idPositionLeave);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
