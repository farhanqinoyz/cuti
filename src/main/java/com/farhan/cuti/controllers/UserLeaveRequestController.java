package com.farhan.cuti.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhan.cuti.dtomodel.BucketApprovalDTO;
import com.farhan.cuti.dtomodel.PositionDTO;
import com.farhan.cuti.dtomodel.PositionLeaveDTO;
import com.farhan.cuti.dtomodel.UserLeaveRequestDTO;
import com.farhan.cuti.dtomodel.UsersDTO;
import com.farhan.cuti.model.BucketApproval;
import com.farhan.cuti.model.Position;
import com.farhan.cuti.model.PositionLeave;
import com.farhan.cuti.model.UserLeaveRequest;
import com.farhan.cuti.model.Users;
import com.farhan.cuti.repositories.BucketApprovalRepository;
import com.farhan.cuti.repositories.PositionLeaveRepository;
import com.farhan.cuti.repositories.PositionRepository;
import com.farhan.cuti.repositories.UserLeaveRequestRepository;
import com.farhan.cuti.repositories.UsersRepository;

@RestController
@RequestMapping("api/requestLeave")
public class UserLeaveRequestController {
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	PositionRepository positionRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@GetMapping("listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> ListRequest(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "totalDataPerPage") Integer perPage,
			@PathVariable(value = "choosenPage") Integer choosenPage){
		ModelMapper modelMapper = new ModelMapper();
		Pageable pageable = PageRequest.of(choosenPage, perPage);
		Page<BucketApproval> listBucket = bucketApprovalRepository.findByReqByUser(userId,pageable);

		List<BucketApprovalDTO> listBucketApprovalDTO = new ArrayList<BucketApprovalDTO>();
		for(BucketApproval bucketApproval : listBucket) {
			BucketApprovalDTO bucketApprovalDTO = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
			listBucketApprovalDTO.add(bucketApprovalDTO);
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Data", listBucketApprovalDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/request")
	public Map<String, Object> UserLeaveRequest(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO){
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequestDTO> listUserLeaveRequestDTO = MappingUserLeaveRequest();
		List<PositionLeave> listPositionLeave = positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<PositionLeaveDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		
		Long differencesDate = Math.abs(userLeaveRequestDTO.getLeaveDateFrom().getTime() - userLeaveRequestDTO.getLeaveDateTo().getTime());
		Long diff = TimeUnit.DAYS.convert(differencesDate, TimeUnit.MILLISECONDS);
		Long maxLeavePerYear = new Long(0);
		Users usersById = usersRepository.findById(userLeaveRequestDTO.getUsers().getIdUser()).get();
		UsersDTO usersDTO = modelMapper.map(usersById, UsersDTO.class);
		Boolean isExistThatYear = false;
		Boolean error = true;
		List<Integer> listRemainingDaysOff = new ArrayList<Integer>();
		Date dateToday = new Date();
		int maxLeave = 0;
		
		for(PositionLeave positionLeave: listPositionLeave) {
			PositionLeaveDTO dto = modelMapper.map(positionLeave, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(dto);
		}
		
		for(PositionLeaveDTO positionLeaveDTO: listPositionLeaveDTO) {
			if(usersDTO.getPosition().getIdPosition() == positionLeaveDTO.getPosition().getIdPosition()) {
				maxLeavePerYear = Long.valueOf(positionLeaveDTO.getMaxLeavePerYear());
			}
		}

		System.out.println("Sebagai : "+usersDTO.getPosition().getNamePosition());
		System.out.println("Maksimal Cuti pertahun : "+maxLeavePerYear);
		
		Date dateLeave= userLeaveRequestDTO.getLeaveDateFrom();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateLeave);
		
		int yearLeave = calendar.get(Calendar.YEAR); // Mengambil tahun
		
		for(UserLeaveRequestDTO dtoUserLeave : listUserLeaveRequestDTO) {
			
			Date dateTableLeave= userLeaveRequestDTO.getLeaveDateFrom();
			Calendar calendarTableLeave = Calendar.getInstance();
			calendarTableLeave.setTime(dateTableLeave);
			int yearTableLeave = calendarTableLeave.get(Calendar.YEAR);
			if(dtoUserLeave.getUsers().getIdUser() == userLeaveRequestDTO.getUsers().getIdUser()) {
				if(yearTableLeave == yearLeave) {
					isExistThatYear = true;
					listRemainingDaysOff.add(dtoUserLeave.getRemainingDaysOff());
				}
			}
			
		}

		
		if(!isExistThatYear) {
			maxLeave = Math.toIntExact(maxLeavePerYear);
		}else {
			maxLeave = Collections.min(listRemainingDaysOff);
		}
		
		if(maxLeave <= 0) {
			result.put("Message", "Mohon maaf, jatah cuti Anda telah habis.");		
		}else if(userLeaveRequestDTO.getLeaveDateFrom().getTime() > userLeaveRequestDTO.getLeaveDateTo().getTime()) {
			result.put("Message", "Tanggal yang anda ajukan tidak valid.");
		}else if(userLeaveRequestDTO.getLeaveDateFrom().getTime() < dateToday.getTime()) {
			result.put("Message", "Tanggal mulai cuti tidak boleh Tanggal yang sudah dilampaui!.");
		}
		else if(maxLeavePerYear < diff) {
			result.put("Message", "Permintaan anda melebihi sisa cuti maksimal anda tahun ini!");
		}else if(maxLeave < diff) {
			result.put("Message", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal"
					+ userLeaveRequestDTO.getLeaveDateFrom()
					+ "sampai "
					+ userLeaveRequestDTO.getLeaveDateTo()
					+ "("+diff+" hari). Jatah cuti Anda yang tersisa adalah "+maxLeave+" hari.");
		}else {
			result.put("Message", "Permohonan anda sedang diproses..");
			error = false;
		}
		
		
		UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
		userLeaveRequest.setSubmissionStatus("waiting");
		userLeaveRequest.setSubmissionDate(dateToday);
		userLeaveRequest.setRemainingDaysOff(maxLeave);
		
		if(!error) {
			userLeaveRequestRepository.save(userLeaveRequest);	
		}
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	public List<UserLeaveRequestDTO> MappingUserLeaveRequest(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepository.findAll();
		List<UserLeaveRequestDTO> listUserLeaveRequestiDTO = new ArrayList<UserLeaveRequestDTO>();
		for(UserLeaveRequest userLeaveRequest : listUserLeaveRequest) {
			UserLeaveRequestDTO userLeaveRequestDTO = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
			listUserLeaveRequestiDTO.add(userLeaveRequestDTO);
		}
		return listUserLeaveRequestiDTO;
	}
	
	public List<BucketApprovalDTO> MappingBucketApproval(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<BucketApproval> listBucketApproval = bucketApprovalRepository.findAll();
		List<BucketApprovalDTO> listBucketApprovaliDTO = new ArrayList<BucketApprovalDTO>();
		for(BucketApproval bucketApproval : listBucketApproval) {
			BucketApprovalDTO bucketApprovalDTO = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
			listBucketApprovaliDTO.add(bucketApprovalDTO);
		}
		return listBucketApprovaliDTO;
	}
}
