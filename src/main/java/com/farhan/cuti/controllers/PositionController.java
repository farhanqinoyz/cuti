package com.farhan.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.farhan.cuti.dtomodel.PositionDTO;
import com.farhan.cuti.model.Position;
import com.farhan.cuti.repositories.PositionRepository;

@RestController
@RequestMapping("api/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPosition(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Position> listPosition= positionRepository.findAll();
		List<PositionDTO> listPositionDTO = new ArrayList<PositionDTO>();
		
		for(Position position : listPosition) {
			PositionDTO dto = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Position success!");
		result.put("Data", listPositionDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePosition(@Valid @RequestBody PositionDTO positionDTO){
		ModelMapper modelMapper = new ModelMapper();
			Position position = modelMapper.map(positionDTO, Position.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Position success!");
		result.put("Data", positionRepository.save(position));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePosition(@Valid @RequestBody PositionDTO positionDTO,
			@PathVariable(value = "id") Long idPosition){

		Map<String, Object> result = new HashMap<String, Object>(); 

		ModelMapper modelMapper = new ModelMapper();
		Position positionById = positionRepository.findById(idPosition).get();
		Date createdDate = positionById.getCreatedDate();
		String createBy = positionById.getCreatedBy();
		
		positionById = modelMapper.map(positionDTO,Position.class);
		
		positionById.setIdPosition(idPosition);
		positionById.setCreatedDate(createdDate);
		positionById.setCreatedBy(createBy);
		
		positionRepository.save(positionById);
		result.put("Status", 200);
		result.put("Message", "Update data Position success!");
		result.put("Data", positionById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idPosition) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Position position = positionRepository.findById(idPosition).get();
    	
        positionRepository.delete(position);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record success!");
		result.put("ID : ",""+idPosition);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
