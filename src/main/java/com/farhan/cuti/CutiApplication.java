package com.farhan.cuti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing 
public class CutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CutiApplication.class, args);
	}
}
