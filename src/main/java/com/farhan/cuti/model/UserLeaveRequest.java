package com.farhan.cuti.model;
// Generated Jan 14, 2020 12:15:33 PM by Hibernate Tools 5.1.10.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * UserLeaveRequest generated by hbm2java
 */
@Entity
@Table(name = "user_leave_request", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdDate", "updatedDate"})
public class UserLeaveRequest implements java.io.Serializable {

	private Long idUserLeaveRequest;
	private Users users;
	private String submissionStatus;
	private Date submissionDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private Integer remainingDaysOff;
	private String description;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private Set<BucketApproval> bucketApprovals = new HashSet<BucketApproval>(0);

	public UserLeaveRequest() {
	}

	public UserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	public UserLeaveRequest(Long idUserLeaveRequest, Users users, String submissionStatus, Date submissionDate,
			Date leaveDateFrom, Date leaveDateTo, Integer remainingDaysOff, String description, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate, Set<BucketApproval> bucketApprovals) {
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.users = users;
		this.submissionStatus = submissionStatus;
		this.submissionDate = submissionDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.remainingDaysOff = remainingDaysOff;
		this.description = description;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.bucketApprovals = bucketApprovals;
	}

	@Id
	@Column(name = "id_user_leave_request", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_user_leave_request_id")
	@SequenceGenerator(name="generator_user_leave_request_id", sequenceName="user_leave_request_id", schema = "public", allocationSize = 1)
	public Long getIdUserLeaveRequest() {
		return this.idUserLeaveRequest;
	}

	public void setIdUserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user")
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "submission_status")
	public String getSubmissionStatus() {
		return this.submissionStatus;
	}

	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "submission_date", length = 13)
	public Date getSubmissionDate() {
		return this.submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_from", length = 13)
	public Date getLeaveDateFrom() {
		return this.leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_to", length = 13)
	public Date getLeaveDateTo() {
		return this.leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	@Column(name = "remaining_days_off")
	public Integer getRemainingDaysOff() {
		return this.remainingDaysOff;
	}

	public void setRemainingDaysOff(Integer remainingDaysOff) {
		this.remainingDaysOff = remainingDaysOff;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 13)
	@CreatedDate
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", length = 13)
	@LastModifiedDate
	public Date getUpdatedDate() {
		return this.updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userLeaveRequest")
	public Set<BucketApproval> getBucketApprovals() {
		return this.bucketApprovals;
	}

	public void setBucketApprovals(Set<BucketApproval> bucketApprovals) {
		this.bucketApprovals = bucketApprovals;
	}

}
