package com.farhan.cuti.dtomodel;

import java.util.Date;

public class BucketApprovalDTO {
	private Long idBucketApproval;
	private UserLeaveRequestDTO userLeaveRequest;
	private String requesterName;
	private Date decisionDate;
	private String resolverReason;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private String description;
	private String status;
	private UsersDTO users;
	public BucketApprovalDTO() {
		super();
	}
	public BucketApprovalDTO(Long idBucketApproval, UserLeaveRequestDTO userLeaveRequest, String requesterName,
			Date decisionDate, String resolverReason, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate, String description, String status, UsersDTO users) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.userLeaveRequest = userLeaveRequest;
		this.requesterName = requesterName;
		this.decisionDate = decisionDate;
		this.resolverReason = resolverReason;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.description = description;
		this.status = status;
		this.users = users;
	}
	public Long getIdBucketApproval() {
		return idBucketApproval;
	}
	public void setIdBucketApproval(Long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}
	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}
	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}
	public String getRequesterName() {
		return requesterName;
	}
	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}
	public Date getDecisionDate() {
		return decisionDate;
	}
	public void setDecisionDate(Date decisionDate) {
		this.decisionDate = decisionDate;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UsersDTO getUsers() {
		return users;
	}
	public void setUsers(UsersDTO users) {
		this.users = users;
	}
	
	
}
