package com.farhan.cuti.dtomodel;

import java.util.Date;

import com.farhan.cuti.model.Users;

public class UserLeaveRequestDTO {
	private Long idUserLeaveRequest;
	private UsersDTO users;
	private String submissionStatus;
	private Date submissionDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private Integer remainingDaysOff;
	private String description;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public UserLeaveRequestDTO() {
		super();
	}
	public UserLeaveRequestDTO(Long idUserLeaveRequest, UsersDTO users, String submissionStatus, Date submissionDate,
			Date leaveDateFrom, Date leaveDateTo, Integer remainingDaysOff, String description, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.users = users;
		this.submissionStatus = submissionStatus;
		this.submissionDate = submissionDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.remainingDaysOff = remainingDaysOff;
		this.description = description;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public Long getIdUserLeaveRequest() {
		return idUserLeaveRequest;
	}
	public void setIdUserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}
	public UsersDTO getUsers() {
		return users;
	}
	public void setUsers(UsersDTO users) {
		this.users = users;
	}
	public String getSubmissionStatus() {
		return submissionStatus;
	}
	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}
	public Date getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
	public Date userLeaveRequestDTO() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public Integer getRemainingDaysOff() {
		return remainingDaysOff;
	}
	public void setRemainingDaysOff(Integer remainingDaysOff) {
		this.remainingDaysOff = remainingDaysOff;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	
}
