package com.farhan.cuti.dtomodel;

import java.util.Date;

public class UsersDTO {
	private Long idUser;
	private PositionDTO position;
	private String nameUser;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public UsersDTO() {
		super();
	}
	
	public UsersDTO(Long idUser, PositionDTO position, String nameUser, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		super();
		this.idUser = idUser;
		this.position = position;
		this.nameUser = nameUser;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public PositionDTO getPosition() {
		return position;
	}
	public void setPosition(PositionDTO position) {
		this.position = position;
	}
	public String getNameUser() {
		return nameUser;
	}
	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
