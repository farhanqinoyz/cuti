package com.farhan.cuti.dtomodel;

import java.util.Date;

public class PositionLeaveDTO {
	private Long idPositionLeave;
	private PositionDTO position;
	private Long idPosition;
	private Integer maxLeavePerYear;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public PositionLeaveDTO() {
		super();
	}
	public PositionLeaveDTO(Long idPositionLeave, PositionDTO position, Long idPosition, Integer maxLeavePerYear,
			String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.position = position;
		this.idPosition = idPosition;
		this.maxLeavePerYear = maxLeavePerYear;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public Long getIdPositionLeave() {
		return idPositionLeave;
	}
	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}
	public PositionDTO getPosition() {
		return position;
	}
	public void setPosition(PositionDTO position) {
		this.position = position;
	}
	public Long getIdPosition() {
		return idPosition;
	}
	public void setIdPosition(Long idPosition) {
		this.idPosition = idPosition;
	}
	public Integer getMaxLeavePerYear() {
		return maxLeavePerYear;
	}
	public void setMaxLeavePerYear(Integer maxLeavePerYear) {
		this.maxLeavePerYear = maxLeavePerYear;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
